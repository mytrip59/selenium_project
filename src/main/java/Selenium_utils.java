import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

public class Selenium_utils {
    public static void loadDataFromTableGrid(Map<String, String[]> myMap, WebDriver driver) {
        for (Map.Entry<String, String[]> item : myMap.entrySet()) {
            By byItemKey = By.xpath(item.getKey());
            if (!item.getValue()[1].equals("tableGridTitle")) {
                String valueItem = driver.findElement(byItemKey).getAttribute("value");
                myMap.put(item.getKey(), new String[]{valueItem, null});
            } else {
                // For counter need to use getText()
                String valueItem = driver.findElement(byItemKey).getText();
                myMap.put(item.getKey(), new String[]{valueItem, null});
            }
            // System.out.println("key: " + item.getKey() + " . Value: " + CNTF1004MapActual.get(item.getKey())[0]);
        }
    }

    public static void loadNameFromTableGrid(Map<String, String[]> myMap, WebDriver driver) {
        for (Map.Entry<String, String[]> item : myMap.entrySet()) {
            By byItemKey = By.xpath(item.getKey());
            String valueItem = driver.findElement(byItemKey).getAttribute("name");
            myMap.put(item.getKey(), new String[]{valueItem, null});
            System.out.println("key: " + item.getKey() + " . Value: " + item.getValue()[0]);
        }
    }

    public static boolean compareTwoMaps(Map<String, String[]> referenceMap, Map<String, String[]> actualMap) {
        boolean result = true;
        for (Map.Entry<String, String[]> item : referenceMap.entrySet()) {
            String referenceValue = null;
            String actualValue = null;
            String fieldName = null;
            referenceValue = item.getValue()[0];
            actualValue = actualMap.get(item.getKey())[0];
            fieldName = item.getValue()[1];
            if (!actualValue.equals(referenceValue)) {
                result = false;
                System.out.println("Field " + fieldName + " is not equal. Reference = '" + referenceValue
                        + "' .Actual result = '" + actualValue + "'");
            }
        }
        return result;
    }

    public static WebElement waitTillNotException(By by, int timeSecWait, WebDriver driver) {
        WebElement webElement = null;
        for (int i = 0; i < 2 * timeSecWait; i++) {
            try {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                webElement = driver.findElement(by);
            } catch (StaleElementReferenceException e) {
                System.out.println("500 msec");

            }
        }
        return webElement;
    }

    public static boolean checkExistReferenceFile(String filePathString) {
        File f = new File(filePathString);
        if (f.exists()) {
            return true;
        } else return false;
    }

    public static Map<String, String> webElementListToMap(List<WebElement> webElementList) {
        Map<String, String> returnMap = new HashMap<>();
        webElementList.forEach(k -> {
            String itemName = k.getAttribute("name");
            String itemValue = k.getAttribute("value");
            returnMap.put(itemName, itemValue);
        });
        return returnMap;
    }

    public static void writeMapToReferenceFileIfNotExist(String filePathString, Map<String, String> myMap) {
        File f = new File(filePathString);
        if (!f.exists()) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(f);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

                objectOutputStream.writeObject(myMap);
                objectOutputStream.close();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            } catch (IOException e) {
                System.out.println("Error initializing stream");
            }

        } else System.out.println("reference.txt is already exist");
    }

    public static Map<String, String> readReferenceFileMap(String filePathString) {
        Map<String, String> myMap = null;
        File f = new File(filePathString);

        if (f.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(filePathString);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

                //Read objects

                myMap = (Map<String, String>) objectInputStream.readObject();

                objectInputStream.close();
                fileInputStream.close();

            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            } catch (IOException e) {
                System.out.println("Error initializing stream");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } else System.out.println("reference.txt is not exist");
        return myMap;
    }

    public static boolean compareActualWebElementListWithReferenceMapTrueAllEqual(
            List<WebElement> actualWebElementList, Map<String, String> referenceMap, boolean displayDifferences) {
        boolean testFailedOrTrue = true;
        if (actualWebElementList.size() != referenceMap.size()) {
            System.out.println("Warning! Reference structure is different from actual data!");
        }

        Map<String, String> actualMap = new HashMap<>();
        actualWebElementList.forEach(k -> {
            actualMap.put(k.getAttribute("name"), k.getAttribute("value"));
        });

        for (Map.Entry<String, String> item : referenceMap.entrySet()) {
            String fieldName = item.getKey();
            String referenceValue = item.getValue();
            String actualValue = actualMap.get(item.getKey());
            if (!actualValue.equals(referenceValue)) {
                testFailedOrTrue = false;
                if (displayDifferences) {
                    System.out.println("Field " + fieldName + " is not equal. Reference = '" + referenceValue
                            + "' .Actual result = '" + actualValue + "'");
                }
            }
        }
        return testFailedOrTrue;
    }

    public static boolean isAlertPresent(WebDriver driver) {
        boolean foundAlert = false;
        WebDriverWait wait = new WebDriverWait(driver, 0 /*timeout in seconds*/);
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            foundAlert = true;
        } catch (TimeoutException eTO) {
            foundAlert = false;
        }
        return foundAlert;
    }
}
