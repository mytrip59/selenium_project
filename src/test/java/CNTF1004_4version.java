import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@Test
public class CNTF1004_4version {
    private WebDriver driver;
    private final By bodyXPath = By.xpath("/html/body");
    private final By allInputElementsXpath = By.xpath("//input [starts-with(@name, 'FormModule:CNTF1004.Block:CNTD') or starts-with(@name, 'FormModule:CNTF1004.Block:ICTS')]");
    private List<WebElement> inputAllWebElements;
    private Map<String, String> firstRowDataMap;

    @BeforeClass
    void launchChrome() {

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        if (driver == null) {
            driver = new ChromeDriver(chromeOptions);
        }

        driver.get("http://192.168.88.213:8083/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement loginField = driver.findElement(By.xpath("//*[@id='i0116']"));
        loginField.sendKeys("epalglobaladmin@epaldev.onmicrosoft.com");

        WebElement NextButton = driver.findElement(By.xpath("//input[@value='Next' and @id='idSIButton9']"));
        NextButton.click();

        WebElement passwordField = driver.findElement(By.xpath("//*[@id='i0118']"));
        passwordField.sendKeys("pastEPAL432101");

        By signInButton = By.xpath("//input[@value='Sign in' and @id='idSIButton9']");
        wait.until(ExpectedConditions.elementToBeClickable(signInButton)).click();


        By staySignedInButtonNo = By.xpath("//*[@id='idBtn_Back']");
        wait.until(ExpectedConditions.elementToBeClickable(staySignedInButtonNo)).click();

        By selectCompanyDropDownMenuBy = By.xpath("//*[@id='ngdialog1']/div[2]/div/form/fieldset/div[2]/div/div/div/div[2]/select");
        Select variableName = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(selectCompanyDropDownMenuBy)));
        variableName.selectByVisibleText("EPAL");

        WebElement selectCompanyOkButton = driver.findElement(By.xpath("//*[@id='ngdialog1']/div[2]/div/form/fieldset/div[3]/div/div/a/span"));
        selectCompanyOkButton.click();

        By contadoresMenu = By.xpath("//*[@id='sidebar']/div/div[1]/ul/li[7]/a/span[2]");
        wait.until(ExpectedConditions.elementToBeClickable(contadoresMenu)).click();

        By consultasMenu = By.xpath("//*[@id='menu_287']");
        wait.until(ExpectedConditions.elementToBeClickable(consultasMenu)).click();

        By contadoresSecundariosMenu = By.xpath("//*[@id='menu_CNTF1004']");
        wait.until(ExpectedConditions.elementToBeClickable(contadoresSecundariosMenu)).click();
    }

    @Test(priority = 1)
    void Test_01_search_all_data_default() {
        // Steps
        Selenium_utils.waitTillNotException(bodyXPath, 20, driver);
        driver.findElement(By.tagName("body")).sendKeys(Keys.F8);

        // load all input elements in List
        inputAllWebElements = driver.findElements(allInputElementsXpath);
        firstRowDataMap = Selenium_utils.webElementListToMap(inputAllWebElements);


        // if reference file is not exist -> need to create
        if (!Selenium_utils.checkExistReferenceFile("src/main/resources/reference.txt")) {
            Map<String, String> referenceMap = Selenium_utils.webElementListToMap(inputAllWebElements);
            Selenium_utils.writeMapToReferenceFileIfNotExist("src/main/resources/reference.txt", referenceMap);
        }

        // compare actual List<WebElement> with reference Map from reference file. If not true -> boolean = false
        Map<String, String> referenceMapFromFile = Selenium_utils.readReferenceFileMap("src/main/resources/reference.txt");
        boolean compareResult = Selenium_utils.compareActualWebElementListWithReferenceMapTrueAllEqual(inputAllWebElements, referenceMapFromFile, true);
        if (!compareResult) {
            throw new AssertionError("Test Test_01_search_all_data_default is failed. Reference and actual data are differenet.");
        }
    }


    @Test(priority = 2, dependsOnMethods = {"Test_01_search_all_data_default"})
    void Test_02_search_all_data_scroll() {
        By scrollBarBottom = By.xpath("//fieldset[@class='block-border canvas-content ng-scope']/div[5]/div[1]/ul/li[3]/span");

        // one click scrolling
        Selenium_utils.waitTillNotException(scrollBarBottom, 10, driver).click();

        try {
            // check the row before clicking was the final or not. Check PopUp window.
            By popUpWindowNotData = By.xpath("//*[@id='alerts-dialog']/fieldset/div[3]/div/div[1]/button/span");
            WebDriverWait wait = new WebDriverWait(driver, 5);
            wait.until(ExpectedConditions.visibilityOfElementLocated(popUpWindowNotData)).click();
            throw new AssertionError("Test_02_search_all_data_scroll is failed. Impossible to check changing data after clicking on the scrollBar. Only one row in the table.");

        } catch (TimeoutException e) {
            // if not PopUp window -> wait for next row and compare
            // compare the next row List<WebElement> with the first row List <WebElement>.
            // true == not differences -> test is failed
            boolean compareResult = Selenium_utils.compareActualWebElementListWithReferenceMapTrueAllEqual(inputAllWebElements, firstRowDataMap, false);
            if (compareResult) {
                throw new AssertionError("Test_02_search_all_data_scroll is failed. After Scrolling all data were the same, without any differences.");
            }
        }
    }

    @AfterClass
    void closeWebdriver() {
        driver.close();
        driver.quit();
    }
}
